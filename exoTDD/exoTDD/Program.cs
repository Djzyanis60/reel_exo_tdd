﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace exoTDD
{
    public class Program
    {
        public static string Saisie(string ma_phrase)
        {
            string reel_phrase;
            if (string.IsNullOrWhiteSpace(ma_phrase))
            {
                Console.WriteLine("Ceci n'est pas une phrase.");
                return null;
            }
            else
            {
                reel_phrase = ma_phrase;
                Console.WriteLine("Votre phrase est {0}", reel_phrase);
                return reel_phrase;
            }
        }

        public static bool Maj(string ma_phrase)
        {
            char premiere_lettre = ma_phrase[0];
            if ((premiere_lettre >= 'A') && (premiere_lettre <= 'Z'))
            {
                Console.WriteLine("Cette phrase commence par une majuscule.");
                return true;
            }
            else
            {
                Console.WriteLine("Cette phrase ne commence pas par une majuscule.");
                return false;
            }
        }

        public static bool Point(string ma_phrase)
        {
            char derniere_lettre = ma_phrase[ma_phrase.Length - 1];
            if (derniere_lettre == '.')
            {
                Console.WriteLine("Cette phrase se termine par un point.");
                return true;

            }
            else
            {
                Console.WriteLine("Cette phrase ne se termine pas par un point.");
                return false;
            }
        }

        public static void Main(string[] args)
        {
            string ma_phrase;
            Console.Write("Saisir une phrase :");
            ma_phrase = Console.ReadLine();

            ma_phrase = Saisie(ma_phrase);
            Maj(ma_phrase);
            Point(ma_phrase);
            Console.ReadKey();
        }
    }
}
