﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using exoTDD;
namespace Test_exoTDD
{
    [TestClass]
    public class TestTDD
    {
        [TestMethod]
        public void TestMajuscule()
        {
            Assert.IsTrue(Program.Maj("Cette phrase est vraie."));
            Assert.IsFalse(Program.Maj("cette phrase est fausse."));
        }
        [TestMethod]
        public void TestPoint()
        {
            Assert.IsTrue(Program.Point("Cette phrase est vraie."));
            Assert.IsFalse(Program.Point("cette phrase est fausse"));
        }
    }
}
